syntax on
set nocompatible

filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#begin()

Plugin 'gmarik/vundle'

Plugin 'kchmck/vim-coffee-script'
Plugin 'scrooloose/nerdtree'

" Plugin 'molokai'
Plugin 'ZenCoding.vim'
Plugin 'nginx.vim'
" Plugin 'zxqfl/tabnine-vim'
Plugin 'codota/tabnine-vim'
" Plugin 'L9'
" Plugin 'FuzzyFinder'
" Plugin 'AutoComplPop'
" Plugin 'Shougo/neocomplcache'
" Plugin 'Diablo3'
" Plugin 'fontsize'
" Plugin 'snipMate'

Plugin 'moonscript-vim'
Plugin 'mattsa/vim-eddie'
Plugin 'vim-markdown'
Plugin 'mustache/vim-mustache-handlebars'
Plugin 'fatih/vim-go'

" Plugin 'ciaranm/inkpot'
" Plugin 'opa'

" Plugin 'ctrlp.vim'
call vundle#end()

filetype plugin indent on

let lua_version = 5
let lua_subversion = 1

let g:go_fmt_command = "goimports"

" let g:neocomplcache_enable_at_startup = 1
" let g:neocomplcache_enable_smart_case = 1
" let g:neocomplcache_enable_underbar_completion = 1
"
" let g:neocomplcache_lock_buffer_name_pattern = '\[fuf\]'
"
"
set guifont=Terminus

if has('gui_running')
	set lines=50
	set columns=100
	" colors wombat
	" colors molokai
	colors eddie
	" colors inkpot
	" set gfn=Consolas:h14:a
	" set guifont=Consolas\ 11
	" set guifont=Droid\ Sans\ Mono\ 15

	" set guifont=Consolas\ 14

	set linespace=-2
	" set guifont=Source\ Code\ Pro\ 13

	set guioptions-=T "hide toolbar
	set go-=m " hide the menu
	set go-=L " hide scroll bars
	set go-=r
	set go-=e
	" use vim tab bar instead of gtk one
	set showtabline=1
else
	colors eddie
	set mouse=a
	if has("nvim")
	else
		set ttymouse=xterm2
	end
end

set ai " auto indent
set nu " line numbers
" set rnu " relative line numbers
"" set ts=4 " tab size
"" set shiftwidth=4 " tab size in auto indent

set shiftwidth=2
set tabstop=2
set softtabstop=2
set et

set binary noeol " don't put cr at end of file
" set showmatch " show matching braces
set backspace=indent,eol,start
set novb " don't use visual bell
set cmdheight=1 " send height of command bar to 1
set laststatus=2
set ruler
set wildmenu
set hlsearch " highlight search
set incsearch " incremental search
set cot+=longest " autocomplete to longest unambiguous string
set gdefault " global replace by default
set complete=.,b

set noswapfile

set ignorecase
set smartcase " ignore case only when everything is lowercase, cool

command W w " big W writes like little w
command Blame !urxvt -e git blame %

" command History !urxvt -e git log -p %
if has("nvim")
	command History vsplit | terminal git log -p %
else
	command History vertical terminal git log -p %
end

if has("nvim")
	autocmd TermOpen * startinsert
end

if !has("nvim")
	set cm=blowfish2
end

map <f1> :noh<cr>

" map <S-f2> :Explore %:p:h<cr>
" map <f2> :tabe %<cr>:Explore %:p:h<cr>

map <S-f2> :NERDTree %:p:h<cr>
map <f2> :tabe %<cr>:NERDTree%:p:h<cr>

nmap <leader>2 :NERDTree %:p:h<cr>
nmap <leader>3 :e %:p:h<cr>

map <f9> :q<cr>
noremap <f12> <esc>:syntax sync fromstart<cr>
inoremap <f12> <c-o>:syntax sync fromstart<cr>

map <c-t> :tabe<cr><c-f>
map <c-left> gT
map <c-right> gt

" tab switching & moving
map <A-1> :tabn 1<cr>
map <A-!> :tabm 0<cr>

map <A-2> :tabn 2<cr>
map <A-@> :tabm 1<cr>

map <A-3> :tabn 3<cr>
map <A-#> :tabm 2<cr>

map <A-4> :tabn 4<cr>
map <A-$> :tabm 3<cr>

map <A-5> :tabn 5<cr>
map <A-%> :tabm 4<cr>

au BufNewFile,BufRead *.as set filetype=as
au BufNewFile,BufRead *.rabl set filetype=ruby

au BufNewFile,BufRead *.css set filetype=scss

au BufNewFile,BufRead *.frag,*.vert,*.fp,*.vp,*.glsl setf glsl

au BufNewFile,BufRead *.flex,*.jflex set filetype=jflex
au BufRead,BufNewFile *.cup set filetype=cup
au BufNewFile,BufRead *.lsl set filetype=lsl
au BufNewFile,BufRead *.less set filetype=less

au BufNewFile,BufRead *.leaf set filetype=leaf
au BufNewFile,BufRead *.es6 set filetype=javascript

au BufNewFile,BufRead *.rockspec set filetype=lua

au BufNewFile,BufRead *.sl set filetype=sl

au BufNewFile,BufRead *.hx set filetype=haxe

au BufNewFile,BufRead Tupfile,*.tup set filetype=tup

au BufNewFile,BufRead *.rl set filetype=ragel

au BufNewFile,BufRead *.md set filetype=mkd
let g:vim_markdown_folding_disabled=1

" use spaces for haskell files
au BufNewFile,BufRead *.hs
	\ set expandtab

" enable html in lua substring
" au BufNewFile,BufRead *.lua
" 	\ unlet b:current_syntax |
" 	\ syn include @luaHtml syntax/html.vim |
" 	\ syn region htmlString start=+\[==\[+ end=+]==]+ keepend contains=@luaHtml

au BufNewFile,BufReadPost *.coffee setl shiftwidth=2 expandtab

au BufNewFile,BufReadPost nginx.conf setf nginx

" don't let ctrl space mess stuff up
imap <Nul> <Space>

imap <c-u> <esc>
" imap <S-Space> _

" rockets and stuff
imap <C-j> ->
imap <C-k> =>

inoremap <C-l> []<esc>i
" inoremap <C-f> ""<esc>i
inoremap <C-Space> ""<esc>i

inoremap <C-f> {{  }}<esc>2hi
inoremap <C-g> ()<esc>i
inoremap <C-d> {<return>}<esc>O

imap <c-e>j <%-  %><esc>2hi
imap <c-e>k <%=  %><esc>2hi

" switch from tabs to spaces
map <f11> :setlocal shiftwidth=4 tabstop=4 softtabstop=4 et<cr>
map <c-f11> :setlocal shiftwidth=2 tabstop=2 softtabstop=2 et<cr>
map <f10> <c-f11>

" java stuff
au BufNewFile,BufRead *.java
	\ map <f3> :JavaImportMissing<cr>|
	\ imap <buffer> <c-k> System.out.println("

au FileType python imap <buffer> <c-k> self

au FileType ruby setlocal shiftwidth=2 tabstop=2 softtabstop=2 et
au FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2 et
au FileType treetop setlocal shiftwidth=2 tabstop=2 softtabstop=2 et
au FileType moon setlocal shiftwidth=2 tabstop=2 softtabstop=2 et
au FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2 et

au FileType php setlocal shiftwidth=4 tabstop=4 softtabstop=4 noet
au FileType scss setlocal shiftwidth=4 tabstop=4 softtabstop=4 et
au FileType cpp setlocal noet
au FileType mkd setlocal shiftwidth=2 tabstop=2 softtabstop=2 et
au FileType scheme setlocal shiftwidth=2 tabstop=2 softtabstop=2 et

au FileType python setlocal shiftwidth=4 tabstop=4 softtabstop=4 et |
	\ setlocal list listchars=tab:▷⋅,trail:⋅,nbsp:⋅

au BufNewFile,BufRead *.erb
	\ setlocal shiftwidth=2 tabstop=2 softtabstop=2 et |
	\ imap <buffer> <c-l> <%=  %><esc>2hi

au BufNewFile,BufRead *.etlua
	\ imap <buffer> <c-j> <%  %><esc>2hi|
	\ imap <buffer> <c-k> <%=  %><esc>2hi|
	\ imap <buffer> <c-l> <%-  %><esc>2hi

" au BufNewFile,BufRead *.moon
" 	\ imap <buffer> <c-m> require("moon").p 

au BufNewFile,BufRead *.lml
	\ setlocal shiftwidth=2 tabstop=2 softtabstop=2 et

au FileType r 
	\ setlocal shiftwidth=2 tabstop=2 softtabstop=2 et |

let html_use_css = 1

" " ctrl-f in insert mode toggles _ for spaces written
" " Failed experiment, I don't use this ever
" let g:spaceToUnderscore = 0
" function! ToggleSpaceToUnderscore()
" 	if g:spaceToUnderscore == 0 | imap <space> _| endif
" 	if g:spaceToUnderscore == 1 | iunmap <space>| endif
" 	let g:spaceToUnderscore = (g:spaceToUnderscore+1) % 2
" endf
" imap <c-f> <c-o>:call ToggleSpaceToUnderscore()<cr>

map <space> <c-w>w
" map <c-f> :FufCoverageFile!<cr>
" map <c-e> :FufBuffer<cr>

map <c-space> gt
map <s-space> gT

" let g:fuf_coveragefile_exclude = '\v\~$|\.(o|exe|dll|bak|orig|swp|pyc|pyo|class|png|gif)$|(^|[/\\])\.(hg|git|bzr)($|[/\\])'
" let g:fuf_file_exclude = '\v\~$|\.(o|exe|dll|bak|orig|swp|pyc|pyo|class|png|gif)$|(^|[/\\])\.(hg|git|bzr)($|[/\\])'

let NERDTreeIgnore=['\.o$', '\~$', '\.pyc$', '\.class$', '\.beam$']

" autocmd InsertEnter * syn clear EOLWS | syn match EOLWS excludenl /\s\+\%#\@!$/
" autocmd InsertLeave * syn clear EOLWS | syn match EOLWS excludenl /\s\+$/
" highlight EOLWS ctermbg=red guibg=red

" strips whitespace from end of lines
nmap <leader>w :%s/\s\+$//<CR>:let @/=''<CR>
nmap <leader>f :noh<cr>
nmap <leader>s :w<cr>
nmap <leader>q :q<cr>
map <c-s> :w<cr>

autocmd FileType python set ft=python.django
autocmd FileType html set ft=htmldjango.html

function! Chomp(str)
	return substitute(a:str, '\n$', '', '')
endfunction

function! DmenuOpen(dmenu_cmd, file_cmd, vim_cmd)
	let fname = Chomp(system(a:file_cmd . " | " . a:dmenu_cmd . " -i -l 20 -p " . a:vim_cmd . " -fn 'Terminus-16'"))
	if empty(fname) || fname =~ '^fatal:'
		return
	endif
	execute a:vim_cmd . " " . fname
endfunction

function! DmenuGit(cmd)
	call DmenuOpen("dmenu", "git ls-files", a:cmd)
endfunction

function! DmenuGitDir(cmd)
	call DmenuOpen("dmenu", "git ls-files | sed 's/[^/]\\+$//' | sed '/^$/d' | sort | uniq", a:cmd)
endfunction

function! DmenuFind(cmd)
	call DmenuOpen("dmenu", "find . -type f", a:cmd)
endfunction

function ItchTranslate()
	return Chomp(system("/home/leafo/code/sites/itch.io/itchio-i18n/insert_string --from " . shellescape(expand("%")) . " " . shellescape(getreg(""))))
endfunction

vnoremap <c-j> x"=ItchTranslate()<cr>p

function ItchTranslateLookup()
  return Chomp(system("/home/leafo/code/sites/itch.io/itchio-i18n/select_key --code"))
endfunction

nnoremap <c-j> "=ItchTranslateLookup()<cr>P

nmap <c-t> :call DmenuGit("tabe")<cr>
nmap <c-f> :call DmenuGit("e")<cr>
nmap <c-g> :call DmenuGitDir("tabe")<cr>

command Find call DmenuFind("e")

set modeline